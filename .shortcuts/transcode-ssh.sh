#!/data/data/com.termux/files/usr/bin/bash

# ToDo: Mejorar la inicialización.
# ~/.secreto
# ```bash
# export user=usuario
# export host=servidor-ssh
# ```
. ~/.secreto

if [[ -n $user && -n $host ]]; then
    ~/transcode.sh ~/sdcard/DCIM/Camera ~/sdcard/DCIM/Transcode
else
    echo '\e[1;31mERROR!\e[0;0m: $user y/o $host no definido(s)' <&2
fi

# Bell.
for i in {1..3}; do
    echo -n $'\007'
    sleep 0.3
done

echo
echo -e "\e[1;36m--- \e[1;32m¡LISTO! \e[1;36m---\e[0;0m"
read -sn1
