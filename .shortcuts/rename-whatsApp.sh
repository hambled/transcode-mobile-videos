#!/data/data/com.termux/files/usr/bin/bash
~/rename.sh ~/sdcard/{DCIM/{Transcode,Done},WhatsApp/Media/"WhatsApp Video"}

# Bell.
for i in {1..3}; do
    echo -n $'\007'
    sleep 0.3
done

echo
echo -e "\e[1;36m--- \e[1;32m¡LISTO! \e[1;36m---\e[0;0m"
read -sn1
