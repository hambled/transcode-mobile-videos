#!/data/data/com.termux/files/usr/bin/bash
ffmpeg -t "$1" -i "$2" -acodec copy -map_metadata 0 "_$2" &&
touch -cr   "$2" "_$2"
