#!/data/data/com.termux/files/usr/bin/bash

# $0 transcode destino [original]
#
# Mueve de una carpeta <transcode> o otra <destino> todos los ficheros mkv y jpg
# y renombra por fecha.
#
# Si se especifica una carpeta <original>, se borrarán de esta carpeta todos los
# ficheros que coincidan con los ficheros de <transcode> antes de haber sido
# renombrada sin tener en cuenta la extensión.
# Autor: McA
# v1.2

# Notas varias:
# Recuperar todas las tags:
#  exiftool -s -G $fichero
#
# # Extraer solo el valor de un tag:
# exiftool -NombreDelTag -s -s -s $fichero
#
# # Pasar el tag DateTimeOriginal de UTC a la hora local.
# date -d $(exiftool -DateTimeOriginal -s -s -s -d @%s $fichero) +%Y-%m-%d_%H-%M-%S

err(){
    echo -e "\e[31m[ERROR]:\e[0m $1" >&2
    exit $2
}
type exiftool &> /dev/null || err "No se pudo encontrar el programa: exiftool" 3
perl -MImage::ExifTool -e 1 &> /dev/null || {
    # apt install make
    e="No se encuentra instalado el modulo de perl Image::ExifTool."
    e+="\n\e[33m[Info]\e[0m: Puedes instalarlo con \e[36m'cpan -i Image::ExifTool'\e[0m"
    err "$e" 3
}


# Extrae la fecha más fiable de entre los tags del vídeo $1.
#
# NOTA 1: Algunos tags se encuentran en UTC, por lo que la hora recuperada
# podría no ser la esperada. Para ello se pasa la hora UTC a la GMT+X según
# la configuración del dispositivo. Esta zona puede controlarse mediante la
# variable $TZ (TimeZone). Por ejemplo:
# export TZ=America/Mexico_City
# Para más opciones se puede consultar /usr/share/zoneinfo/**.
#
# NOTA 2: Esta pensada para ser usada con tags de ficheros Matroska (mkv), pero
# puede funcionar igualmente con otros tags.
# Para otros tags consultar: https://exiftool.org/TagNames/
get_date(){
    local i date
    for i in DateTimeOriginal CreateDate FileModifyDate; do
        date=$(exiftool -$i -s -s -s -d %s "$1" 2>/dev/null)
        [[ $date -gt 0 ]] && break
    done
    date -d @$date +%Y-%m-%d_%H-%M-%S
}

# Añade números al final del nombre del fichero ($1)
# hasta que no exista o sea igual a $2 (shasum).
unico(){
    local nom=${1%.*}
    local ext=${1##*.}
    local z=0
    local ret=$1
    local sha=$(shasum "$2"|sed 's/\( \+\).*/\1/')

    while [[ -f $ret ]]; do
        echo "$sha$ret" | shasum -cs && break
        z=$(($z + 1))
        ret=${nom}_$z.$ext
    done
    echo -n "$ret"
}

dir_t="${1%/}"      # Transcode.
dir_d="${2%/}"      # Destino.
dir_o="${3%/}"      # Original.

[[ -d $dir_t ]] || err "No se ha especificado el directorio: transcode" 1
[[ -d $dir_d ]] || err "No se ha especificado el directorio: destino"   2

echo -e "Origen: \e[31m$dir_t\e[0m"
echo -e "Destino: \e[32m$dir_d\e[0m"

while read i; do
    echo -n "$i: "
    old="$dir_t/$i"
    datef=$(get_date "$old")
    dst=$(dirname "$i")/$datef.${i##*.}
    new=$dir_d/$dst
    new=$(unico "$new" "$old")
    if [[ -f $new ]]; then
        echo -e "Ya existe \e[31m$new\e[0m."
        rm "$old"
    else
        echo -e "Moviendo a \e[32m$dst\e[0m."
        mkdir -p "${new%/*}"
        mv "$old" "$new"
    fi
    [[ $? -ne 0 || ! -d $dir_o ]] || {
        rm -f "$dir_o/${i%.*}".*
    }
done < <(                  \
    find "$dir_t"          \
    -type f \(             \
        -iname "*.jpg"  -o \
        -iname "*.jpeg" -o \
        -iname "*.mkv"  \) \
    -printf %P\\n)
