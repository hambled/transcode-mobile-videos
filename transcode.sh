#!/data/data/com.termux/files/usr/bin/bash

# Transcodifica uno o más videos en una carpeta por defecto o específica
# preservando el timestap de cada vídeo.
#
# origen := { video | carpeta_de_videos }
# $0 origen [[origen...] carpeta_destino]
#
# La extensión va definida en la variable $video_ext.
#
#
# Si se usa transcodificación por SSH hay que usar las variables:
# - $user: Usuario remoto.
# - $host: Host remoto.
# IMPORTANTE!: Falta hacer los checks.
#
# v1.3
# Autor: McArio

video_ext="${video_ext:-mp4}"

# Default dst folder.
[[ -d ${@: -1} ]] || set "$@" ~/sdcard/DCIM/Transcode

ffmpeg_local(){
    ffmpeg              \
      -hide_banner      \
      -loglevel warning \
      -y                \
      -i -              \
      -c:v libx265      \
      -c:a aac          \
      -b:a 320k         \
      -map_metadata 0   \
    "$1" &> /dev/null
}

ffmpeg_ssh(){
    ssh $user@$host       \
      ffmpeg              \
        -hide_banner      \
        -loglevel warning \
        -i -              \
        -c:v libx265      \
        -c:a aac          \
        -b:a 320k         \
        -map_metadata 0   \
        -f matroska -     \
    > "$1"
}

# Transcodifica un vídeo en otra carpeta.
# En la carpeta de destino ($2) se creará un fichero con el código de
# error y el nombre del fichero de origen con la ruta canonicalizada.
#
# transcode <fichero origen> <ruta destino>
transcode(){
    local ret
    local dst_file
    [[ -f $1 ]] || return 1
    [[ -d $2 ]] || return 2

    dst_file="$2"/"${1##*/}"
    dst_file="${dst_file%.*}.mkv"

    # Si existe el fichero y no está vacío.
    [[ -s $dst_file ]] && return 3

    pv "$1" | if [[ -n $user && -n $host ]]; then
        ffmpeg_ssh   "$dst_file"
    else
        ffmpeg_local "$dst_file"
    fi &> /dev/null
    ret=$?

    [[ $ret -gt 0 ]] && {
        rm -rf "$dst_file"
        return $ret
    }

    # Copia las referencias de tiempo.
    echo  "$ret: $1" >> "$2"/done.txt

    touch -cr   "$1" "$dst_file"
    return $?
}

dst_folder=$(readlink -f "${@: -1}")
[[ -d $dst_folder ]] || exit 2

unset folders files

# Pasar los parametros a 2 vectores.
# Uno de ficheros y otro de carpetas.
for (( i=1; i<$#; i++ )); do
    tmp=$(readlink -f "${!i}")
    [[ -f $tmp && $tmp =~ \.$video_ext$ ]] &&   files+=("$tmp")
    [[ -d $tmp                          ]] && folders+=("$tmp")
done

# Buscar todos los vídeos de cada carpeta y
# subcarpeta y pasarlos al vector de ficheros.
for i in "${folders[@]}"; do
    while read f; do
        files+=("$f")
    done < <(find "$i" -type f -name "*.$video_ext")
done

# Transcodificar todos los vídeos encontrados.
for i in "${files[@]}"; do
    transcode "$i" "$dst_folder" ||
        echo "Ocurrió algún error: $?." >&2
done
